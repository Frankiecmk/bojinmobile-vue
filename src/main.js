import Vue        from 'vue'
import App        from './App'
import router     from './router'
import store      from './store'
import ajx        from './server/AjaxPromise.js' // 不要删这一行
import vueLayer   from './plugins/vueLayer'
import * as utils from '@/utils'
import * as Filter from '@/filter'
import VueClipboard from 'vue-clipboard2' // 点击复制组件
Vue.config.productionTip = false

// 组件加载

import {Loadmore, Spinner} from 'mint-ui'

Vue.use(VueClipboard)

// 注册?
Vue.component(Spinner.name, Spinner)
Vue.component(Loadmore.name, Loadmore)

window.__DEBUG = true
window.store = store
window.appRouter = router
// 开发 debug 模式
// ajax 模拟 数据
AppRun()

function AppRun() {
  vueLayer()

  let {delay} = utils

  LoadData(function () {
    let {OnEvent, delay} = utils
    delay(() => {
      OnEvent() // 回调数据
    }, 0)
  })
}

/**
 * 标记头部    window.store.state.home.path = window.router.app._route.name
 * 路由的信息  window.AppRouter.app._route
 * 初始化     首页数据 查询公告消息
 * @param    cb 0965132833
 * @constructor
 */
function LoadData(cb) {
  let {baseLoad} = utils;
  /* eslint-disable no-new */
  baseLoad(function(){
    window.MainApp = new Vue({
      el: '#app',
      router, store,
      render: h => h(App)
    })
  },store);
  cb()
}
/**
 aa123456
 syw123456

 正式网登录

 */
