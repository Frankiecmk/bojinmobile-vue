// 首页初始化数据
let queryIndex = {
  no: 0,
  data: {
    eGameOpen:[ // 开放的电子游戏
        { cname: 'MG电子',
          id:'mg',
          plus:true
        },
        { cname: 'AG电子',
          id:'ag',
          news: true
        },
        { cname: 'BBIN电子',id:'bbin',},
        { cname: 'og电子',id:'og',},
      ]
  }
}

let account = {
  no: 0,
  data: {
/*     account: {
       "account": "huiyuan01",
       "money": "",
     }*/
  }
}

let login = {
  no: 0,
  data: { // 成功 登陆
/*    account: {
      "account": "huiyuan01",
      "money": "",
    }*/
  }
}
let hotGameList = {
  no: 0,
  data: [
    {
      name: '电子热门游戏',
      type: 'egame',
      data:[
        {
          "cname": "樱桃1",
          "ename": "falaowang2",
          "gcode": "falaowang2",
          "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
          "web_url": "http:\/\/www.woaibojin.com\/pt_frame.php",
          'gameType': 'pp',
          "isHot": true,
        }
      ]
    },{
      name: '真人热门游戏',
      type: 'live',
      data:[
        {
          "cname": "OG",
          "ename": "falaowang2",
          "gcode": "falaowang2",
          "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
          "web_url": "http:\/\/www.woaibojin.com\/pt_frame.php",
          'gameType': 'pp',
          "isHot": true,
        },
        {
          "cname": "AG",
          "ename": "falaowang2",
          "gcode": "falaowang2",
          "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
          "web_url": "http:\/\/www.woaibojin.com\/pt_frame.php",
          'gameType': 'pp',
          "isHot": true,
        },
      ]
    },{
      name: '彩票热门游戏',
      type: 'lottery',
      data:[
        {
          "cname": "合1",
          "ename": "falaowang2",
          "gcode": "falaowang2",
          "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
          "web_url": "http:\/\/www.woaibojin.com\/pt_frame.php",
          'gameType': 'pp',
          "isHot": true,
        }
      ]
    }
  ]
}

let activityList = {
  no: 0,
  data: {
    list: [
      {
        'activityType': '1',
        'name': '体育',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '体育0.4%周返水，无上下限',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '1',
        'name': '体育',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': 'AE专属，30%笔笔送',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '2',
        'name': '娱乐城',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '8000元见面礼，开门行大运',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '2',
        'name': '娱乐城',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '积分换现金',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '3',
        'name': '彩票',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': 'AG首存30%，倾情献上',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '4',
        'name': '老虎机',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '千百倍，赢钱再送',
        'content': '<p>活动对象：贝赢全体会员<br />↵活动场所：全平台<br />↵活动内容：</p>↵↵<table border="1" cellpadding="0" cellspacing="0" style="width:80%">↵	<tbody>↵		<tr>↵			<td style="text-align:center">选择方式</td>↵			<td style="text-align:center">被邀请人存款</td>↵			<td style="text-align:center">邀请人奖励</td>↵			<td style="text-align:center">被邀请人奖励</td>↵			<td style="text-align:center">流水倍数</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式一</td>↵			<td style="text-align:center">500</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">88</td>↵			<td style="text-align:center">12</td>↵		</tr>↵		<tr>↵			<td style="text-align:center">方式二</td>↵			<td style="text-align:center">1000</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">168</td>↵			<td style="text-align:center">12</td>↵		</tr>↵	</tbody>↵</table>↵↵<p>申请方式：<br />↵联系在线客服提供邀请人账号和被邀请人账号即可。<br />↵<br />↵活动规则：<br />↵1.邀请人需累积存款最低200元且被邀请人自注册时间起，一周内累计存款最低500元方可申请推荐奖励。<br />↵<br />↵2.<strong>被邀请人</strong>注册时间需<strong>晚于邀请人注册</strong>时间且<strong>被邀请人不能在代理合营商下注册 </strong>，否则无法申请推荐好友奖励。<br />↵<br />↵3.申请推荐好友奖励<strong>只可选择方式一或方式二，</strong>即同一被邀请人仅可被邀请一次。<br />↵<br />↵4.推荐奖金可投注全平台产品，完成12倍流水方可提款。<br />↵<br />↵5.有效投注不包括所有视频扑克游戏，刮刮乐，基诺游戏，所有Pontoon游戏，各种Craps游戏，赌场战争游戏，娱乐场Hold&#39;em游 戏，牌九游戏，多旋转老虎机以及老虎机奖金翻倍投注。如投注此类任何游戏将取消申领资格，扣除所有红利以及赢利。<br />↵<br />↵6.本活动每一位会员、每一电子邮箱、每一电话号码、每一设备号码、相同银行账户及IP地址只能有一个会员账户可享受。<br />↵<br />↵7.贝赢保留对本活动的最终解释权。</p>'
      },
      {
        'activityType': '5',
        'name': '老虎机',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '通关奖励，周额外双返水'
      },
      {
        'activityType': '5',
        'name': '老虎机',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '火线救援， 助您逆境重生'
      },
      {
        'activityType': '5',
        'name': '老虎机',
        'imgUrl': '/static/images/activity/10f59b5c-c474-4fb9-8ccd-26e2074aae83.jpg',
        'activityTitle': '每日特色，轻松中大奖'
      }
    ]
  }
}


hotGameList.data[0].data.push(hotGameList.data[0].data[0])
hotGameList.data[0].data.push(hotGameList.data[0].data[0])
hotGameList.data[0].data.push(hotGameList.data[0].data[0])
hotGameList.data[0].data.push(hotGameList.data[0].data[0])
hotGameList.data[0].data.push(hotGameList.data[0].data[0])
hotGameList.data[0].data.push(hotGameList.data[0].data[0])

hotGameList.data[1].data.push(hotGameList.data[0].data[0])
hotGameList.data[1].data.push(hotGameList.data[0].data[0])
hotGameList.data[1].data.push(hotGameList.data[0].data[0])
hotGameList.data[1].data.push(hotGameList.data[0].data[0])
hotGameList.data[1].data.push(hotGameList.data[0].data[0])
hotGameList.data[1].data.push(hotGameList.data[0].data[0])


hotGameList.data[2].data.push(hotGameList.data[0].data[0])
hotGameList.data[2].data.push(hotGameList.data[0].data[0])
hotGameList.data[2].data.push(hotGameList.data[0].data[0])
hotGameList.data[2].data.push(hotGameList.data[0].data[0])
hotGameList.data[2].data.push(hotGameList.data[0].data[0])
hotGameList.data[2].data.push(hotGameList.data[0].data[0])


queryIndex.data.hotGameList = hotGameList.data



let eGame = {
  "no": 0,
  "data": [
    {
        "cname": "pt老虎机",
        "ename":"pt",
        "games": [
          {
            "cname": "法老王II1",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "法老王II2",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "法老王II3",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "法老王II4",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "法老王II5",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }]
      },
    {
        "cname": "mg电子",
        "ename" : "mg",
        "games": [
          {
            "cname": "樱桃小丸子1",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "樱桃小丸子2",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "樱桃小丸子3",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "樱桃小丸子4",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }, {
            "cname": "樱桃小丸子5",
            "ename": "falaowang2",
            "gcode": "falaowang2",
            "img": "http:\/\/images.uxgaming.com\/TCG_GAME_ICONS\/AE\/AE0013.png",
            "web_url": ""
          }]
      }
  ]
}
let messageList = {
  "no": 0,
  "data": [
    {
        "messageid": "115188",
        "type":"0",
        "date":"5月9日",
        "title":"体育维护通知",
        "content":"亲爱的客户，您好，我们体育将于5月10号的13:30-18:30进行维护，请大家调整时间，奔走相告，等维护完成后再来秀球技哟~"
    },
    {
        "messageid": "115188",
        "type":"0",
        "date":"5月9日",
        "title":"体育维护通知",
        "content":"亲爱的客户，您好，我们体育将于5月10号的13:30-18:30进行维护，请大家调整时间，奔走相告，等维护完成后再来秀球技哟~"
    },
    {
        "messageid": "115188",
        "type":"0",
        "date":"5月9日",
        "title":"体育维护通知",
        "content":"亲爱的客户，您好，我们体育将于5月10号的13:30-18:30进行维护，请大家调整时间，奔走相告，等维护完成后再来秀球技哟~"
    }
  ]
}
let transferRecord = {
  "no": 0,
  "data": [
    {
        "date": "2018/5/12 14:53:09",
        "amount":"102.00",
        "type":"存款",
        "orderNo":"AIN1805121453b9fq1",
        "status":"等待"
    },
    {
        "date": "2018/5/12 14:53:09",
        "amount":"102.00",
        "type":"存款",
        "orderNo":"AIN1805121453b9fq1",
        "status":"等待"
    },
    {
        "date": "2018/5/12 14:53:09",
        "amount":"102.00",
        "type":"存款",
        "orderNo":"AIN1805121453b9fq1",
        "status":"等待"
    }
  ]
}

let apiData = {
  'api/query/index': queryIndex,
  'api/query/account': account,
  'api/query/hotGameList': hotGameList,

  'api/login': login,
  'api/getActivityList': activityList,
  'api/getMessageList': messageList,
  'api/getTransferRecord': transferRecord,
}

function name() {

}

export {
  apiData
}
