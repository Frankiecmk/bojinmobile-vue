/*
 公用函数
 */


import * as status from "./statusCode";
import {pagezh} from '@/config/zh.js' // 中文标题
import QRCode from 'qrcode'
let {statusCode} = status
console.log(statusCode, 'statusCode')

function checkLogin() {
	return MainApp.$store.state.home.isLogin ? true : false
}

// 需要登陆的时间 判断
function guestAlert() {
	// 请先登录或注册账号
	if (!MainApp.$store.state.home.isLogin) {
		window.layer.msgWarn('请先登录或注册账号！')
	}
	return false
}

function statusCheck(code, cb) {
	try {
		if (code.no == statusCode.no1 || code.no == statusCode.no2) {
			// ok

			return true
		}
	} catch (e) {

	}
	return false
}

// 跳转函数
function jumpUrl(name, that = null, conditions = null) {
	let _map      = {
		'index': '/wap/index',
		'reg': '/wap/reg',
		'help': '/wap/help',
		'egame': '/wap/egame',
		'forgetPassword': '/wap/forgetPassword',

		'phoneRetrieve': '/wap/phoneRetrieve',
		'mg': '/wap/mg',
		'pt': '/wap/pt',
		'download': '/wap/download',
		// 需要登陆的情况
		'login': '/wap/login',
	}
	// 需要登陆 才能访问
	let _loginMap = {
		'activity': '/wap/activity',
		'memberIndex': '/wap/memberCenter/index',
		'management': '/wap/memberCenter/management',
    'invite': '/wap/memberCenter/invite',
    'rescue': '/wap/memberCenter/rescue',
		// 游戏记录 存取款记录
		'record': '/wap/record',
		'recordGame': '/wap/recordGame',

		'transfer': '/wap/memberCenter/transfer',
		'deposit': '/wap/memberCenter/deposit',
		'bankCard': '/wap/memberCenter/bankCard',
		'bankCardAdd': '/wap/memberCenter/bankCardAdd',
		'password': '/wap/memberCenter/password',
		//
		'withdrawal': '/wap/memberCenter/withdrawal',

		'message': '/wap/message'

	}
	if (_loginMap[name] && !checkLogin()) {
		//
		leftClose()
		MainApp.$router.push(_map['login'])
		return false
	}

	let _mapAll = window._.extend(_map, _loginMap)
	let a       = _mapAll[name]
	if (a) {
		leftClose()
		if (that) {
			that.$router.push(a)
		} else {
			MainApp.$router.push(a)
		}

	}
	return false
}

// 扩充 vue { } 减少代码量
// 同名 无效
function extendVue(arr = null, vueCord) {

	let MethodAll = {}
	_.each(arr, (value, key) => {
		if (MethodAll[key]) {
			_FN[key] = MethodAll[key]
		}
	})
	let _FN = {}

	return _.extend(_FN, vueCord)
}

// 登陆权限控制
/**
 *
 * @param that
 * @param _switch
 *        true 需要登陆， 没登陆 就搞走
 * @returns {string}
 * @constructor
 */
function Authority(that = null, _switch = false) {
	// tdo debug
	// return ''  // 这里return 掉 就不会限制了
	if (!that) return false

	if (_switch) { // 要登陆

		if (!that.$store.state.home.isLogin) {
			jumpUrl('index', that)
			return false
		}
	} else {
		// 不要登陆权限的， 如果登陆 就走
		if (that.$store.state.home.isLogin) {
			jumpUrl('index', that)
			return false
		}
	}
	return true
}

function forbidPath(path) {

}

/*
 *
 * $('body').on('click', '[rel^=lightbox]', function(){});
 *
 * 绑定全局事件
 * */
var nowdeg = 0;

function OnEvent() {
	console.log('OnEvent ') // page="forgetPassword"
	let _map = ['management', 'memberIndex', 'record', 'transfer',
		'deposit', 'index', 'forgetPassword', 'login', 'activity', 'bankCard',
		'record', 'recordGame','invite','rescue',
		'bankCardAdd', 'withdrawal', 'password', 'phoneRetrieve',
		'message',
	]

	window._.each(_map, function (val, key) {
		window.$('body').on('click', '[page="' + val + '"]', function () {
			jumpUrl(val)
		});
	})

	window.$('body').on('click', '.pageback_bt', function () {
		leftOpen()
	});
	window.$('body').on('touchmove', '#barrier_layer', function () {
		event.preventDefault();
		leftClose()
	});
	window.$('body').on('click', '#barrier_layer', function () {
		event.preventDefault();
		leftClose()

	});

	window.$('body').on('click', '#more_func', function () {
		if (nowdeg < 448) {
			$(".more_menu_bg").show();
			$(".more_menu_box").slideDown();
			var interval = setInterval(function () {
				nowdeg = nowdeg + 15;
				$("#more_func_img").css('transform', 'rotate(' + nowdeg + 'deg)');
				if (nowdeg >= 448) {
					clearInterval(interval);
				}
			}, interval);
		} else {
			more_menuClose()
		}
	});
}

function more_menuClose() {
	$(".more_menu_box").slideUp('normal', function () {
		$(".more_menu_bg").hide();
	});
	var interval = setInterval(function () {
		nowdeg = nowdeg - 15;
		$("#more_func_img").css('transform', 'rotate(' + nowdeg + 'deg)');
		if (nowdeg <= 0) {
			clearInterval(interval);
			nowdeg = 0;
			$("#more_func_img").css('transform', 'rotate(' + 0 + 'deg)');
		}
	}, interval);
}


function leftOpen() {
	$(".mobile_homebody_leftmenu").animate({width: '590px'}, 150);
	$("#barrier_layer").show();
	// $('#app').addClass('stop')
}

function leftClose() {
	$("#barrier_layer").hide();
	$(".mobile_homebody_leftmenu").animate({width: '0px'}, 150);
	// $('#app').removeClass('stop')
}

function delay(fn, time = 0) {
	setTimeout(function () {
		fn()
	}, time)
}

function pingKeep() {
	let userinfo = false
	try {
		userinfo = JSON.parse(localStorage['userinfo'])
	} catch (e) {
	}
	if (userinfo && !window.pingID) {
		window.pingID = setInterval(() => {
			window.AjaxPromise({
				Action: '/api/ping.php',
				Requirement: {'user_id': userinfo.id}
			}).then(function (res) {
			})
		}, 30000 * 60)
	}
}

// time 判断
function timeTXT() {
	let now  = new Date(),
		hour = now.getHours()
	let txt  = ''
	if (hour < 6) {
		txt = "凌晨好！"
	} else if (hour < 9) {
		txt = "早上好！"
	} else if (hour < 12) {
		txt = "上午好！"
	} else if (hour < 14) {
		txt = "吃顿丰盛的午餐，为身体加加油"
	} else if (hour < 17) {
		txt = "下午好！"
	} else if (hour < 19) {
		txt = "傍晚好！"
	} else if (hour < 24) {
		txt = "看球时间别忘了来铂金体育投两把！"
	}
	return txt
}


// -------------------------------------------------------------------
// 保存用户信息 传入数据必须是可用的
function saveUser(res, state) {
	state.isLogin            = true
	localStorage['userinfo'] = JSON.stringify(res)
	state.userData           = res
}

function eraseUser(state) {
	console.log('eraseUser------------------------------------------')
	//
	delete localStorage['userinfo']
	delete localStorage['session_id']
	try {
		state.isLogin = false
	} catch (e) {
	}
}

function baseLoad(callback, store) {
	pingKeep()
	loadPic([
		location.origin + '/static/images/ico/icon_03.png'
	])
	// state
	store.state.home.timeTXT = timeTXT()
	
    bankZh(); //
	store.dispatch('notice_list.action', {}).then(res => {
		// console.info(res, '初始化 首页数据')
	}).then(res => {

	})
	// 查询用户数据
	let userinfo = false
	try {
		userinfo = JSON.parse(localStorage['userinfo'])
	} catch (e) {
	}

	if (userinfo && !userinfo.user_name) {
		//
		eraseUser(state)
		callback() // run
		return '' // 没有登陆 todo 清空数据什么的
	}
	store.dispatch('account.action', {
		'user_id': userinfo.id
	}).then(res => {
	//	console.info(res, '查询用户数据 首页数据')
		callback()
	}).then(res => {

	})
	// 首页轮播图
	store.dispatch('initData.action', {})

	// 方法 挂在到 window
	window.Loading = Loading;

// 电子游戏 预加载数据
	/**
	 *  全部来 数据量太大了
	 *
	 *  单个来 可以循环
	 *  todo  数据量太大了 屏蔽掉
	 */
	/*	delay(() => {
	 if (window.$.isEmptyObject(localStorage.eGameList)) {
	 delete localStorage.eGameMd5
	 delete localStorage.eGameList
	 }
	 store.dispatch('egame.action', {'md5': localStorage.eGameMd5 ? localStorage.eGameMd5 : ''}).then(res => {
	 console.info(res, '查询用户数据 首页数据')
	 }).then(res => {

	 })
	 }, 400)*/

}

function markPage() {
	delay(() => {
		let {name: path} = MainApp.$route
		try {
			window.MainApp.$store.state.home.path = path
		} catch (e) {
		}
		try {
			console.info([path], '----------')
			window.MainApp.$store.state.home.title = pagezh[path]
		} catch (e) {
			window.MainApp.$store.state.home.title = ''
		}
		$('.main_act').attr('pageid', path)
		$('#app').attr('headID', path);
		more_menuClose()
    autoLogin(MainApp.$route.query)
	}, 8)

}
//          proxy_pass http://apidev.88bojinshidai.net/;
function bankZh(){
  if(localStorage.bankZh && localStorage.bankZh.length > 12){
    return ''
  }
  window.AjaxPromise({
    Action: '/api/get_user_bank_list.php',
    Requirement: {}
  }).then(function (res) {
    if (statusCheck(res)) {
        localStorage.bankZh = JSON.stringify(res.data)
    //  debugger
    } else {
    //  reject(null, res)
    }
  }, function (res) {})

}

// auto login
function autoLogin(query){
  console.info('query-----', query)
  let {autologin,username} = query
  if(autologin  && username){
    // 可以拿到  window.MainApp.$store  // 可以直接调用了
    // store.dispatch('autologin.action', {
    window.MainApp.$store.dispatch('autologin.action', {
      'url': location.href,
      'username': username,
      'autologin': autologin
    }).then((res)=>{
     //   debugger
    })

/*    window.AjaxPromise({
      Action: '/api/autologin.php',
      Requirement: {

      }
    }).then(function (res) {
    //  console.log(res);
      // 需要调 注册 逻辑
      checkAutoGo(res)
    })*/
  }
}
// 目前只跳 ibc
function checkAutoGo(res){
  if(res.data  && res.data.autogo){
    var goTarget = res.data.autogo
  }
    if(goTarget == 'ibc'){
    // 开始走

    }

 // debugger
}

/**
 *  https://m.woaibojin.com/wap/index?username=bjtest2&autologin=41fabefba63b028d4035e2c175c802f5
 *   ?username=bjtest2&autologin=41fabefba63b028d4035e2c175c802f5
 * @param data
 * @returns {boolean}
 * @constructor
 */

function Negate(data) {
	return !data
}

// 第三方跳转处理  添加game_type参数是为了区分ag真人与ag捕鱼
function thirdPartyFn(type, channel = null,game_type) {
	// 检查下登陆情况
	if (!MainApp.$store.state.home.isLogin) {
		jumpUrl('login')
		return false
	}
	switch (type) {
		case 'alone': // 游戏单独跳转 写在这里 channel = currentGame

            console.log('alone---->');
            console.log(channel);  // pt  mg ttg dt
			window.open(window.location.origin + `/wap/thirdParty?platform=${channel}`).focus()
			break;
		default:
			// bbin 761city  AG // 通用
            console.log('not----alone---->');
            window.open(window.location.origin + `/wap/thirdParty?type=${type}&game_type=${game_type}&channel=${channel}`).focus();
			break

		//  break
	}
}

function userID() {
	let userinfo = {}
	try {
		userinfo = {'user_id': JSON.parse(localStorage['userinfo']).id}
	} catch (e) {
	}
	return userinfo
}

function thirdDock() {

}

var format  = require('date-fns/format')
var addDays = require('date-fns/add_days')

// 查询会使用到
function searchDate(day = 1) {
	var a = {
		// start_time=2018-01-01&end_time=2018-09-01
		end_time: format(new Date(), 'YYYY-MM-DD') // format(new Date(), 'YYYY-MM-DD')

		// api bug add code format(addDays(new Date(), 1), 'YYYY-MM-DD')
	}

	if (day == 1) {
		a.start_time = a.end_time
	} else {
		day *= -1
		a.start_time = format(addDays(new Date(), day), 'YYYY-MM-DD')
	}
	return a
}


// 加载到全部里面
window.loadingDetail = {

}
/*
* uuid == keep will keep loading
*/
var Loading = {
	open: function (uuid) { // 可能被重复调用
	  //if(store.state.home.Loading) return ''
    if(uuid){
      window.loadingDetail[uuid] = true;
    }
		store.state.home.Loading = true
	},
	close: function (uuid) {
		if(uuid != 'keep' && window.loadingDetail['keep']) {
           return ''
		}
	    if(uuid && window.loadingDetail[uuid]){ // if not close it all
	      delete window.loadingDetail[uuid];
	      if(window.$.isEmptyObject(window.loadingDetail)){
	        store.state.home.Loading = false // 最后一个 关闭掉
	      }
	      return ''
	    }else {
	      store.state.home.Loading = false
	      window.loadingDetail = {} // 归零
	    }
	},
}

function LoadingOpen(uuid) {
	// 消除重复 遮罩层
	if (uuid) {
		//
	}
}
// 支付会使用 canvas 保存 二维码
function canvasDownload(id,imgName){

  let canvasID = document.getElementById(id)

  let type = 'png';   //设置下载图片的格式

  let img_png_src = canvasID.toDataURL("image/png");  //将canvas保存为图片

  let imgData = img_png_src.replace(imgType(type),'image/octet-stream');

  let filename = imgName + ' ' +format(new Date(), 'YYYY-MM-DD HH_mm_ss') + '.' + type; //下载图片的文件名

  // 调用保存函数
  saveFile(imgData,filename)

  function imgType(ty) {
    let type = ty.toLowerCase().replace(/jpg/i, 'jpeg');
    var r = type.match(/png|jpeg|bmp|gif/)[0];
    return 'image/' + r;
  }


}

function saveFile(data, fileName, openTab = null){
  let save_link = document.createElement('a');
  save_link.id= 'saveit'
  save_link.href = data;
  save_link.download = fileName;

  let event = document.createEvent('MouseEvents');
  event.initEvent("click", true, false);
  // 设置事件监听.

  //  target="_blank"
  if(openTab){
    save_link.target = '_blank'
  }

  save_link.addEventListener('click', function (e) {
    // e.target 就是监听事件目标元素
    console.log(save_link)
    e.preventDefault();
  }, false);
  save_link.dispatchEvent(event);
}

// 图片直接保存 --》 二维码
function imgDownload(url,imgName){
  // id == url
  // 调用保存函数
  saveFile(url,imgName,'blank') // 打开新tab
}
// url --> 二维码
function QRCodeShow(id,converUrl){
  let canvasID = document.getElementById(id)
  QRCode.toCanvas(canvasID, converUrl,function (error) {
    if (error) console.error(error)
  })
}
// url --> 二维码 直接就是图
function QRCodeImgShow(url,converUrl){
   window.$('#canvasdiv').hide();

   window.$('#canvasimg').attr('src',url).removeClass('hide')

}
// 使用 localStorage +  MD5 方式 控制本地数据 版本
/**
 *
 * @param key  localStorage 的key
 * @param md5  api 返回数据的 md5
 *             默认格式  localStorage[key + '_' + md5 ] = 存放 md5值
 */
function lsMD5(key, md5) {

}

/**
 *   集中处理 请求 支付接口 返回的数据
 *
 *   走不通的逻辑
 *
 *
    if (res.data.is_old) {
            window.layer.msgWarn(res.msg)
          }

 */

function payHanld(that,res){
  // 保存一份原始对象
  var resCopy = JSON.parse(JSON.stringify(res))



  // 方便操纵 直接拿data 值
  var res = res.data

  //1 判断 是否有重复订单
  if(res.is_old){

    /**
     * mode: "redirect"
     */
    if(res.mode == "redirect"){ // 走到第三方
      that.$router.push('/wap/memberCenter/orderDetail3')
      return ''
    }

    // 2 判断 重复订单 是调 二维码 还是 具体信息展示的
      if(res.payment_way == "transfer"){ // 转账类型 跳转路由走
        that.$router.push('/wap/memberCenter/orderDetail')
        return ''
      }

    if(res.mode == 'sta_qrcode'){
      // alert('静态二维码')
      //  debugger
      that.modal_on = true // 打开 弹窗
      let urlCode = res.card_no
      QRCodeImgShow(urlCode)
      // todo 先注释
      //   window.$('#showmsg').html('温馨提示: 请在' + res.timeout_secs  +'秒内完成支付,否则存款金额无法到帐');
    }

    if(res.mode == 'dyn_qrcode'){
      // alert('动态二维码')
      // that.scanURL  = true todo
      that.modal_on = true
      // todo  id 先固定 可能后续 需要动态
      let urlCode = res.card_no
      QRCodeShow('canvasdiv',urlCode)
    }

      console.log('重复订单------------- ',res.payment_way)
  }

  // 初始化 二维码 html
  window.$('#canvasimg').attr('src','')
  window.$('#showmsg').html();

  if(res.mode == 'bank_card' && res.payment_way == 'transfer'){
    // 转账类型 跳转路由走
    that.$router.push('/wap/memberCenter/orderDetail')
    return ''
  }

  if(res.mode == 'show'){
    // 显示弹窗 todo 这个没搞好
    that.scanURL  = res.data.card_no
  }
  if(res.mode == 'sta_qrcode'){
    // alert('静态二维码')
  //  debugger
    that.modal_on = true // 打开 弹窗
    let urlCode = res.card_no
    QRCodeImgShow(urlCode)
    // todo 先注释
 //   window.$('#showmsg').html('温馨提示: 请在' + res.timeout_secs  +'秒内完成支付,否则存款金额无法到帐');
  }

  if(res.mode == 'dyn_qrcode'){
    // alert('动态二维码')
   // that.scanURL  = true todo
    that.modal_on = true
    // todo  id 先固定 可能后续 需要动态
    let urlCode = res.card_no
    QRCodeShow('canvasdiv',urlCode)
  }

  if(res.mode == 'redirect'){ // 这种是要跳走的 到第三方的
    window.location.href
  //  window.open(res.url)

    if(res.card_no){
      window.location.href =  res.card_no
      return ''
    }
    if(res.url){
      window.location.href =  res.url
    }
  }
  if(!res.is_old) {
		if(res.payment_way == "transfer"){ // 转账类型 跳转路由走
        that.$router.push('/wap/memberCenter/orderDetail')
        return ''
      }
  }

}
// 计算 倒计时 时间
var getRemainTime = (lastTime) => {
  var t = lastTime
  var seconds = Math.floor(t % 60)
  var minutes = Math.floor((t  / 60) % 60)
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24)
  var days = Math.floor(t / (1000 * 60 * 60 * 24))
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  }
}

function loadPic(arr = []){
	var imgWrap = [];
	for(var i =0; i< arr.length ;i++) {
		imgWrap[i] = new Image();
		imgWrap[i].src = arr[i];
	}
}


export {
	extendVue,
  Loading,
	Negate, userID,
	statusCheck, searchDate, getRemainTime,
	jumpUrl,
	guestAlert,
	timeTXT,
	markPage,
	// 事件绑定
	OnEvent,
	delay,
	baseLoad,
// pay 相关的 函数
  QRCodeShow,canvasDownload,imgDownload,payHanld,

	// 第三方跳转处理
	thirdPartyFn,
	thirdDock,
	// 保存操作
	saveUser,
	eraseUser,
	Authority
}
