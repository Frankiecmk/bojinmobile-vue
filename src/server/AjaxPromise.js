import $ from 'jquery'
import * as API from '../debug/api.js'
import {Loading} from '@/utils'

window.$    = $
let apiData = API.apiData

function AjaxPromise(param = {Action: '', Requirement: ''}) {
  // 使用Promise封装 Ajax
  return new Promise((resolve, reject) => {
    let ajaxUrl = ''
    if (window.__DEBUG) {
      ajaxUrl = param.Action
    } else {
      ajaxUrl = '/' + param.Action
    }
    var uuid = new Date().getTime()

    if (param.Action == '/api/deposit.php' || param.Action == '/api/login.php' || param.Action == "/api/user_bank_card_list.php") {
      window.Loading.open(uuid)
    }
    let Actions = param.Requirement || {}

    Actions = window._.extend(Actions, {session_id: localStorage.session_id})
    $.ajax({
      url: ajaxUrl,
      type: param.type || 'post',
      dataType: 'json',
      data: Actions,
      success: function (data) {
        resolve(data)
        window.Loading.close(uuid)

        if(!window.MainApp) return ''
   //     debugger
        if ( window.MainApp.$route.path == '/wap/reg' || window.MainApp.$route.path == '/wap/index') {
          // todo 登陆超时
        } else {
          try {
            if (data.no == 301) {
            //  console.log(window.appRouter)
                window.MainApp.$store.state.home.userData = '';
                window.MainApp.$store.state.home.isLogin = false;
               delete localStorage.userinfo
               delete localStorage.session_id
              window.appRouter.push('/wap/login')
                    window.layer.msgWarn("请先登录");
              //
            }
          } catch (e) {
          }
        }
      },
      error: function (error) {
        window.Loading.close(uuid)
        console.info('--api 挂了 error')
        reject(error)
      }
    })
  }).catch(function (err) {
    window.layer.msgWarn('服务超时请稍后再试', function () {})
    if(uuid){
      window.Loading.close(uuid)
    }
    console.info(err, '--api 挂了')
  })
}

window.AjaxPromise = AjaxPromise

otherJS()

function otherJS() {
  $('[name="return"]').on('click', function () {
    MainApp.$router.push('wap/index')
    console.info('1232')
  })
}

export default {}
