let pagezh = {
  'index': '',
  "/wap/memberCenter/index": '个人中心',
  "/wap/memberCenter/management": '个人信息',
  'invite': '邀请好友',
  'activity': '优惠活动',
  'rescue': '自助奖金',
  'pt': 'PT 电子',
  'mg': 'MG 电子',
  'dt': 'DT 电子',
  'ttg': 'TTG 电子',
  'message': '消息',
  'password': '修改密码',
  'withdrawal': '提款',
  'live': '真人视讯',
  'bankCardAdd': '添加银行卡',
  'bankCard': '银行卡管理',
  'transfer': '钱包转账',
  'management': '个人中心',
  'record': '交易记录',
  'recordGame': '投注记录',
  'recordGameList': '投注详情',
  '/memberCenter/bankpay': '银行卡转账',
  'transferRecord': '交易详情',
  '/memberCenter/deposit': '网银支付',
  'payHead': '存款',
  'wxpay': '存款'
}
let payZh = {
  'manpay': '网银支付',
  'onlinepay': '网银支付',
  'swiftpay': '快捷支付',
  'alipay': '支付宝',
  'wxpay': '微信支付',
  'qqpay': 'QQ支付',
  'unionpay': '银联支付',
  'jdpay': '京东支付'
}
let payment_type_code = { //payment_type_code
  'jdpay': '京东支付',
  'alipay': '支付宝',
  'wxpay': '微信支付',
  'qqpay': 'QQ支付',
}

let history = {
  canceled:'取消',
  doing: '处理中',
  timeout: '超时',
  failed: '失败',
  succ: '成功',
  done: '成功'
}
/*let bankZH  = false
bankZH = JSON.parse(localStorage.bankZh)*/

export {pagezh, payZh,payment_type_code,history}
