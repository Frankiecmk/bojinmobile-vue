import * as types from './type'
import * as utils from '@/utils'

let {statusCheck} = utils


export const actions = {
  /**
   * 电子游戏查询 考虑 归类到 首页信息 里面去
   * 不在 电子游戏页面 在去请求数据
   * supplier=&
   */
  [types.INITEGAME]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/game_list.php?device=mobile',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          commit(types.INITDATA_DETAIL, window._.extend(res,{
            options:param
          }))
          resolve(res)
        } else {
          console.info('数据 失败')
          reject(null, res)
        }
      })
    }),

  /* 获取首页平台 supplier_list_m.php */
  [types.SUPPLIER_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/supplier_list_m.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          // 屏蔽 非  ename : "ibc" 的东西

          var one = window._.filter(res.data, (val, key) => {
              //return val.ename == 'ibc'
              return val.ename
          })
          res.data = one

          commit(types.SUPPLIER_DETAIL, res)
          resolve(res)
        } else {
          reject(null, res)
        }
      }, function (res) {})
    }),

  /** 首页 轮播图 */
  [types.INITDATA]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/get_banner.php?device=mobile',
        Requirement: {
          // 'list': param
        }
      }).then(function (res) {
        console.log(res)
        if (statusCheck(res)) {
          commit(types.BANNER_DETAIL, res)
          // 首页 轮播图处理
          resolve(res)
        } else {
          console.info('数据 失败')
          reject(null, res)
        }
      })
    }),
  // 用户信息 查询
  [types.ACCOUNT]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/userinfo.php',
        Requirement: param
      }).then(function (res) {
        console.log(res, '查询用户信息')
        if (statusCheck(res)) {
          commit(types.ACCOUNT_DETAIL, res)
          // 数据处理 如果不处理就放过
          resolve(res)
        } else {
          console.info('types.ACCOUNT 数据 失败')
          reject(null, res)
          commit(types.ACCOUNT_DETAIL, 'err')
        }
      }, function (res) {

      })
    }).catch((err, res) => {
      console.info(err, res, 'get reject')
    }),
  // 自动登录
  [types.AUTOLOGIN_ATION]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/autologin.php', //
        Requirement: param
      }).then(function (res) {
        window.MainApp.$store.state.home.regResult = res;
        if (!res) return resolve()
        if (statusCheck(res)) {
          commit(types.LOGIN_MUTATION, res)
          console.log(res, '自动登录 返回数据')
          resolve(res)
        } else {
          window.layer.msgWarn(res.msg)
          reject(res, res)
        }
      })
    }).catch((err, res) => {
      console.info(err, res, 'get reject')
    }),
  //
  [types.LOGIN_ACTION]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/login.php', //
        Requirement: param
      }).then(function (res) {
        window.MainApp.$store.state.home.regResult = res;
        if (!res) return resolve()
        if (statusCheck(res)) {
          commit(types.LOGIN_MUTATION, res)
          console.log(res, '登陆接口返回数据')
          resolve(res)
        } else {
          console.info('login.php 数据 失败')
          window.layer.msgWarn(res.msg)
          reject(res, res)
        }
      })
    }).catch((err, res) => {
      console.info(err, res, 'get reject')
    }),
  //获取优惠活动列表
  [types.GET_ACTIVITY_LIST]: ({commit}, param = {}, that = null) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/promotion/list.php',
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        res = {
          no: 0,
          data: res
        }
        if (statusCheck(res)) {
          commit(types.GET_ACTIVITY_LIST_MUTATION, res)
          console.log(res, '获取优惠活动列表返回数据')
          resolve(res)
        } else {
          console.info('数据 失败')
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),
  //获取 公告信息
  [types.NOTICE_LIST]: ({commit}, param = {}, that = null) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/notice_list.php',
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          commit(types.NOTICE_LIST_MUTATION, res, that)
          resolve(res)
        } else {
          if (!res.msg) res.msg = '数据 失败 notice_list.php'
          // window.layer.msgWarn(res.msg) todo
          reject(null, res)
        }
      })
    }).catch(() => {
    }),
  //获取收件箱列表
  [types.GET_MESSAGE_LIST]: ({commit}, param = {}, that = null) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/sysmail_list.php',
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          commit(types.GET_MESSAGE_LIST_MUTATION, res, that)
          console.log(res, '获取收件箱列表返回数据')
          resolve(res)
        } else {
          console.info('数据 失败')
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),
  //获取 存款 取款 记录
  [types.GET_TRANSFER_RECORD]: ({commit}, param = {}, that = null) =>
    new Promise((resolve, reject) => {
      console.log(param)
      var url = param.supplier
      var postUrl = `/api/${url}.php`
      if(url == 'my'){
        postUrl = '/api/promotion/my.php'
      }
      window.AjaxPromise({
        Action: postUrl,
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        if(url == 'my' && res.length){
          var res  = {
            no:0,
            data: res
          }
        }
        if (statusCheck(res)) {
          commit(types.GET_TRANSFER_RECORD_MUTATION, res, that)
          resolve(res)
        } else {
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),
}
