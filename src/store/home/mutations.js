import * as types from './type'
import * as utils from '@/utils'


export const state = {
  title: '', // 标题
  path: '', // 当前路径 url
  timeTXT: '',
  isLogin: false,
  Loading: false, // 加载 转圈
  hotGameList: [], // 首页 游戏 list
  eGameList: [],
  userData: {},
  notice: [],
  regResult: {},
  activityList: [],
  supplier: [],
  bannerImg: [],
  transferRecord: [], // 交易记录
  onLineChat: 'https://v66.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=80000146&configID=34#'
}
export const mutations = {
  /** 电子游戏 信息
   *  修改下 具体传递参数
   *  暂时不用了
   * */
  [types.INITDATA_DETAIL](state, res) {

    // 检查参数
    let supplier = res.options.supplier
    let md5 = res.options.md5

    if (res.options.md5 == res.md5) { // 本地数据已经是最新的 return 下
      return ''
    }
    if (supplier) { // 查询 某个 游戏类型
      if (window.$.isEmptyObject(res.data)) { //　remove it
        localStorage['eGameList_' + supplier] = '' // 清空本地的数据
        //  localStorage['eGameList_' + supplier + '_md5'] = '' md5 值留着
      } else { // 空= 查询全部

        localStorage['eGameList_' + supplier] = JSON.stringify(res.data[supplier.toUpperCase()])
        localStorage['eGameList_' + supplier + '_md5'] = res.md5
      }

    } else {
      // 全部的数据 现在不要这里的数据

    }

  },
  /** 首页 img 轮播图 */
  [types.BANNER_DETAIL](state, res) {
    if (!window.$.isEmptyObject(res.data)) { // 非空
      state.bannerImg = res.data
    }
  },
  /** 首页 游戏信息 img 跳第三方 */
  [types.SUPPLIER_DETAIL](state, res) {
    if (!window.$.isEmptyObject(res.data)) { // 非空
      state.supplier = res.data
    }
  },

  /** 账号信息 */
  [types.ACCOUNT_DETAIL](state, res) {
    // 统一入口 保存数据
    let {saveUser, eraseUser} = utils
    //
    res == 'err' ? eraseUser(state) : saveUser(res.data, state)
    try {
      console.log(state.isLogin, 'state.isLogin', res.data.account)
    } catch (e) {
    }
  },

  /** 账号登陆获取信息 */
  [types.LOGIN_MUTATION](state, res) {
    if (res.data && res.data.user_name) {
      state.isLogin = true
      localStorage['session_id'] = res.session_id
      localStorage['userinfo'] = JSON.stringify(res.data)
      state.userData = res.data

      var url = '/wap/index'
      if(res.data.autologin){
        var autogo  = res.data.autogo
        localStorage['autogo'] = autogo; // 本地跳走到ibc
      }else {
        // todo 登陆 和账号 查询回来的 字段是否一致
      }
      window.MainApp.$router.push(url)
    }
  },

  /** 获取优惠活动列表 */
  [types.GET_ACTIVITY_LIST_MUTATION](state, res) {
    if (res.data && res.data[0]) {
      state.activityList = res.data
    }
  },
  /** 公告信息 */
  [types.NOTICE_LIST_MUTATION](state, res) {
    if (res.data && res.data[0]) {
      state.notice = res.data
    }
  },
  /** 获取收件箱列表 */
  [types.GET_MESSAGE_LIST_MUTATION](state, res) {

  },
  /** 获取交易记录 */
  [types.GET_TRANSFER_RECORD_MUTATION](state, res) {
    if (res.data && res.data[0]) {
      state.transferRecord = res.data
    }
  },
  /* change passworld*/

  [types.PASSWORD_MUTATION](state, res) {
    let {jumpUrl} = utils
    jumpUrl('memberIndex')
    // 修改密碼的 後續操作
  }



}
