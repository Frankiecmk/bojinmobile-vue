export const INITDATA = 'initData.action'

// 电子游戏 处理 目前没用了
export const INITDATA_DETAIL = 'initData.detail'

export const BANNER_DETAIL = 'banner.detail'


export const ACCOUNT = 'account.action'
export const ACCOUNT_DETAIL = 'account.detail'

//  登陆
export const LOGIN_ACTION = 'login.action'
export const LOGIN_MUTATION = 'login.mutation'


//  优惠活动 查询
export const GET_ACTIVITY_LIST = 'active.action'
export const GET_ACTIVITY_LIST_MUTATION = 'active.mutation'

// 公告信息
export const NOTICE_LIST = 'notice_list.action'
export const NOTICE_LIST_MUTATION = 'notice_list.mutation'

//  收件箱 查询
export const GET_MESSAGE_LIST = 'message.action'
export const GET_MESSAGE_LIST_MUTATION = 'message.mutation'

//  交易记录 查询
export const GET_TRANSFER_RECORD = 'transferRecord.action'
export const GET_TRANSFER_RECORD_MUTATION = 'transferRecord.mutation'

// 首页游戏列表
export const SUPPLIER_ATION = 'supplier.action'
export const SUPPLIER_DETAIL = 'supplier.mutation'


//  电子游戏 INITEGAME
export const INITEGAME = 'egame.action'
export const EGAME_MUTATION = 'egame.mutation'

// 自动登录 autologin
export const AUTOLOGIN_ATION = 'autologin.action'

// 获取银行对照列表
export const BANKZH_ATION = 'bankzh.action'
