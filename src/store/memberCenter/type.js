// 查询平台 余额
export const PLATFORM_ATION = 'platform.action'
export const PLATFORM_MUTATION = 'platform.muaction'

// 刷行 余额 refresh_Balance  单独刷 和全部刷
export const REFREHS_ATION = 'refresh.action'
export const REFREHS_MUTATION = 'refresh.muaction'




export const UPDATA_ATION = 'update_user.action'
export const UPDATA_MUTATION = 'update_user.muaction'

export const USERBANKLIST_ATION = 'user_bank_list.action'
export const USERBANKLIST_MUTATION = 'user_bank_list.muaction'


//user_bank_card_list 获取 用户 绑定的 银行卡列表
export const USER_BANK_ATION = 'bank.action'
export const USER_BANK_MUTATION = 'bank.muaction'

// 添加 银行卡
export const ADD_BANK_ATION = 'add_bank_card.action'

// 支持绑定的 银行卡列表
export const BANK_LIST_ATION = 'bank_card.action'
export const BANK_LIST_MUTATION = 'bank_card_list.action'


// 取款
export const WITHDFRAW_ATION = 'withdraw.action'

export const name = 'name.action'


// 修改密码
export const PASSWORD_CHANGE = 'password.action'
export const PASSWORD_MUTATION = 'password.muaction'

// 获取游戏平台 列表 -> 查询游戏数据
export const RECORDGAME_ATION = 'recordGame.action'
export const RECORDGAME_MUTATION = 'recordGame.muaction'


// recommend_friend 邀请好友
export const RECOMMEND_ATION = 'recommend.action'
