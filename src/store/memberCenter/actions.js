import * as types from './type'
import * as utils from '@/utils'

let {statusCheck}        = utils

export const actions = {
  /* 获取平台 money 个人中心 platform.action */
  [types.PLATFORM_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/balance.php', // todo 接口 还在调整
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          commit(types.PLATFORM_MUTATION, window._.extend(res,{
            options:param
          }))
          resolve(res)
        } else {
          reject(null, res)
        }
      }, function (res) {})
    }),

  [types.UPDATA_ATION]: ({commit}, param) => // 刷一边新的用户数据
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/update_user.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          commit(types.REFREHS_MUTATION, res, param)
          resolve(res)
        } else {
          window.layer.msgWarn(res.msg, function () {})
        }
      }, function (res) {
        reject(res)
      })
    }),
  // user_id=1&session_id=df552b0ed8f46962bbb6322974bd6072
  [types.USER_BANK_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/user_bank_card_list.php',
        Requirement: param
      }).then(function (res) {
        res.param = param
      //  debugger
        if (statusCheck(res)) {
          commit(types.BANK_LIST_MUTATION, res)
          resolve(res)
        } else {
          reject(null, res)
        }
      }, function (res) {
        reject(res)
      })
    }),
  // 添加银行卡
  [types.ADD_BANK_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/add_bank_card.php',
        Requirement: param
      }).then(function (res) {
        res.param = param
        if (statusCheck(res)) {
          commit(types.USER_BANK_MUTATION, res)
          resolve(res)
        } else {
          reject(null, res)
        }
      }, function (res) {
        reject(res)
      })
    }),

  // 可银行卡 列表
  [types.BANK_LIST_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/user_bank_list.php',
        Requirement: param
      }).then(function (res) {
        res.param = param
        if (statusCheck(res)) {
          commit(types.USER_BANK_MUTATION, res)
          resolve(res)
        } else {
          reject(null, res)
        }
      }, function (res) {
        reject(res)
      })
    }),

  /**
      取款
   */
  [types.WITHDFRAW_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/withdraw.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          // commit(types.REFREHS_MUTATION, res) // todo 方法没写好 暂时不用了
          resolve(res)
         window.layer.msgWarn('取款申请已提交成功');
        } else {
            window.layer.msgWarn(res.msg)
       //   reject(null, res) // 异常没有捕获到 这里不捕获异常
        }
      }, function (res) {
        reject(res)
      })
    }),
/*  // 修改 密码
  [types.WITHDFRAW_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/update_passwd.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {

          resolve(res)
        } else {
          reject(null, res)
        }
        return ''
      }, function (res) {
        reject(res)
      })
    }),*/
  /**
   * 游戏记录（平台列表） + 具体的游戏记录
   * @param commit
   * @param param
   * @returns {Promise<any>}
   * http://api.woaibojin.com:8088/betting_list.php?user_id=1&supplier=&session_id=df552b0ed8f46962bbb6322974bd6072
   */
  [types.RECORDGAME_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/betting_list.php',
        Requirement: param
      }).then(function (res) {
      //  debugger
        if (statusCheck(res)) {
          commit(types.RECORDGAME_MUTATION, window._.extend(res,{
            options:param
          }))
          resolve(res)
        } else {
          window.layer.msgWarn(res.msg, function () {})
        }
        return ''
      }, function (res) {
        reject(res)
      })
    }),

  /**
   *  change passworld
   */
  [types.PASSWORD_CHANGE]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/update_passwd.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          commit(types.PASSWORD_MUTATION, res)
          window.layer.msgWarn('修改密码成功', function () {
            MainApp.$router.push('/wap/memberCenter/index')
          })
        } else {
          window.layer.msgWarn(res.msg, function () {})
        }
        resolve(res)
        return ''
      }, function (res) {
        reject(res)
      })
    }),
  // RECOMMEND_ATION
  [types.RECOMMEND_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/promotion/recommend_friend_info.php',
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {

        } else {
        
        }
        resolve(res)
        return ''
      }, function (res) {
        reject(res)
      })
    }),
  
}
