import * as types from './type'
import * as utils from '@/utils'

let {statusCheck} = utils
let urlMap = {
  dt :'/api/game_action/dt.php',
  pt :'/api/game_action/pt.php',
  mg :'/api/game_action/mg.php',
  //gameId
  bbin: '/api/game_action/bbin.php',
  ttg: '/api/game_action/ttg.php',
  '761city': '/api/game_action/761city.php',
  'ibc': '/api/game_action/ibc.php',
  'ebet': '/api/game_action/ebet.php'
}

export const actions = {
  // 公用跳转函数
  [types.GAME_ATION]: ({commit}, param) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: urlMap[param.gameAPI],
        Requirement: param
      }).then(function (res) {
        if (statusCheck(res)) {
          commit(types.CITY761_MUTATION, res)
          resolve(res)
        } else {
          window.layer.msgWarn(res.msg + res.debug_info, function () {})
          reject(null, res)
        }
      }, function (res) {
      })
    })
}
