import * as types from './type'
import * as utils from '@/utils'

let {statusCheck} = utils

export const actions = {
  
  [types.SEND_TXT]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/reg.php', // 获取注册显示的字段
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          resolve(res);
          console.info(res)
        } else {
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),
  // reg
  [types.REG_GET]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/reg.php', // 获取注册显示的字段
        Requirement: param
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          resolve(res);
          window.layer.msgWarn(res.msg,()=>{
           // window.router.push('/wap/login')
          })
          console.info(res)
        } else {
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }).catch((err,res)=>{
      console.info(err,res, 'get reject')
    }),
  
  
}
