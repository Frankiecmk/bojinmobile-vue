import Vue  from 'vue'
import Vuex from 'vuex'
import home from './home'
import memberCenter from './memberCenter'
import reg from './reg'
import thirdParty from './thirdParty'
import record from './record'
import pay from './pay'
Vue.use(Vuex)
/**
 *  ajax 集中起来
 */
export default new Vuex.Store({
  // 將整理好的 modules 放到 vuex 中組合
  modules: {
    home,reg,memberCenter,thirdParty,record,pay
  },
  strict: false
})
