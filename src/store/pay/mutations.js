import * as types from './type'

export const state = {
  payList: [],
  payType: {
    'manpay': {data: [],noData:true},
    'onlinepay': {data: [],noData:true},
    'swiftpay': {data: [],noData:true},
    'alipay': {data: [],noData:true},
    'wxpay': {data: [],noData:true},
    'qqpay': {data: [],noData:true},
    'unionpay': {data: [],noData:true},
    'jdpay': {data: [],noData:true}
  } // 支付平台 开放了那些 内容
}
export const mutations = {
  /**　pay lsit */
  [types.GET_PAYMENT_M](state, res) {
    if (!window.$.isEmptyObject(res.data)) { // 非空
      state.payList = res.data
      try { // 数据保存到本地
        localStorage.payList = JSON.stringify(res.data)
      } catch (e) {
      }
      window._.each(res.data, (val, key) => {
        if (!state.payType[val.payment_type_code]) {
          state.payType[val.payment_type_code] = {data: []}
        } else {
          state.payType[val.payment_type_code].data.push(val)
        }
      })
      window._.each(state.payType, (val, key) => {
        if (!val.data.length) {

          val.noData = true
        }else{
          val.noData = false
        }
      })
      localStorage.get_payment = JSON.stringify(state.payType)
    }
  },

}
