/*
*    get_payment.php
*


命名 pay.xx
 * */
// 获取 通道
export const GET_PAYMENT = 'get.payment'
export const GET_PAYMENT_M = 'get.payment.m'
/**

 'onlinepay': '网银支付', 提交支付


 */
export const PAY_ONLINEPAY  = 'pay.onlinepay'
export const PAY_ONLINEPAYS  = 'pay.onlinepayS'
//  'swiftpay': '快捷支付'
export const PAY_SWIFTPAY  = 'pay.swiftpay'

// 'alipay': '支付宝',
export const PAY_ALIPAY  = 'pay.alipay'

//  'wxpay': '微信支付',
export const PAY_WXPAY  = 'pay.wxpay'

//  'qqpay': 'QQ支付',
export const PAY_QQPAY  = 'pay.qqpay'

//  'unionpay': '银联支付',
export const PAY_UNIONPAY  = 'pay.unionpay'

//  'jdpay': '京东支付'
export const PAY_JDPAY = 'pay.jdpay'

//CTO Billy, [11.06.18 21:38]
//http://api.woaibojin.com:8088/deposit_cancel.php?id=301314&user_id=16&session_id=8d788f435c60f0787b9ae43d2360539f 取消存款
//  'jdpay': '京东支付'
export const PAY_CANCEL = 'pay.cancel'
