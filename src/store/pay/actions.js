import * as types from './type'
import * as utils from '@/utils'

let {statusCheck} = utils

let cparam = function(){
  return {
    device_type: 'mobile',
    user_id: JSON.parse(localStorage['userinfo']).id,
    session_id: localStorage['session_id'],
  }
}

export const actions = {

  [types.GET_PAYMENT]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/get_payment.php', // 支付通道
        Requirement: window._.extend(param,cparam(),{}) // 合并参数
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          commit(types.GET_PAYMENT_M, res)
          resolve(res);
        } else {
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),

	[types.PAY_ONLINEPAY]: ({commit}, param = {}) =>
		new Promise((resolve, reject) => {
			window.AjaxPromise({
				Action: '/api/deposit.php', // 支付通道
				Requirement: window._.extend(param,cparam(),{
				//	payment_type_code: 'onlinepay' // 默认都是这个
				}) // 合并参数
			}).then(function (res) {
				if (!res) return resolve()
				if (statusCheck(res)) {
          try { // 支付通道 数据数据保存到本地 防止刷新 拿不到值
            localStorage.depositRes = JSON.stringify(res)
          } catch (e) {
          }
				//	commit(types.GET_PAYMENT_M, res)
					resolve(res);
				} else {
					window.layer.msgWarn(res.msg)
					reject(null, res)
				}
			})
		}),

  [types.PAY_CANCEL]: ({commit}, param = {}) =>
    new Promise((resolve, reject) => {
      window.AjaxPromise({
        Action: '/api/deposit_cancel.php', // 支付通道
        Requirement: window._.extend(param,cparam(),{ }) // 合并参数
      }).then(function (res) {
        if (!res) return resolve()
        if (statusCheck(res)) {
          //	commit(types.GET_PAYMENT_M, res)
          resolve(res);
        } else {
          window.layer.msgWarn(res.msg)
          reject(null, res)
        }
      })
    }),
}
