import index      from './../components/center'
import management from './../components/center/management'
import record     from './../components/center/record'
import recordGame from './../components/center/record/game'

// 邀请好友
import invite from './../components/center/invite'

import transferRecord from './../components/center/record/transferRecord'
import recordGameList from './../components/center/record/recordGameList'

import transfer from './../components/center/transfer'
import bankpay  from './../components/center/deposit/bankpay'

import deposit     from './../components/center/deposit'
import bankCard    from './../components/center/bankCard'
import bankCardAdd from './../components/center/bankCardAdd'
import withdrawal  from './../components/center/withdrawal'
import password    from './../components/center/password'

import alipay    from './../components/center/deposit/alipay'
import jdpay     from './../components/center/deposit/jdpay'
// 网页支付
import onlinepay from './../components/center/deposit/onlinepay'
import payHead   from './../components/center/deposit/payHead'
import qqpay     from './../components/center/deposit/qqpay'
import swiftpay  from './../components/center/deposit/swiftpay'
import orderDetail  from './../components/center/deposit/orderDetail'
import orderDetail3  from './../components/center/deposit/orderDetail3'

import wxpay     from './../components/center/deposit/wxpay'
// 微信扫码
import wxpayScan from './../components/center/deposit/wxpay_scan'
//网银支付 快捷支付页面
import quickpay  from './../components/center/deposit/quickpay'


import thirdParty from './../components/thirdParty'
// 公用扫码支付
import scanpay from './../components/center/deposit/scanpay'
import rescue  from "../components/center/rescue"

//取消订单
import cancelSuccess  from "../components/center/deposit/cancelSuccess";

export default {
  centerRoutes: [
    {
      path: '/wap/memberCenter/index',
      component: index,
      name: '/wap/memberCenter/index'
    },
    {
      path: '/wap/memberCenter/management',
      component: management,
      name: '/wap/memberCenter/management'
    },
    { // 转账
      path: '/wap/memberCenter/transfer',
      component: transfer,
      name: 'transfer' //page-param="transfer"
    },
    { // 银行卡 管理
      path: '/wap/memberCenter/bankCard',
      component: bankCard,
      name: 'bankCard'
    },
    {
      path: 'rescue',
      component: rescue,
      name: 'rescue'
    },
    { //
      path: '/wap/memberCenter/bankCardAdd',
      component: bankCardAdd,
      name: 'bankCardAdd'
    },
    { //invite
      path: '/wap/memberCenter/invite',
      component: invite,
      name: 'invite'
    },
    //
    { //
      path: '/wap/memberCenter/withdrawal',
      component: withdrawal,
      name: 'withdrawal'
    },
    { //
      path: '/wap/memberCenter/bankpay',
      component: bankpay,
      name: 'bankpay'
    },
    { // 修改密码
      path: '/wap/memberCenter/password',
      component: password,
      name: 'password'
    },
    {
      path: '/wap/record',
      component: record,
      name: 'record'
    },
    {
      path: '/wap/recordGame',
      component: recordGame,
      name: 'recordGame'
    },
    {
      path: '/wap/recordGameList', // 具体的游戏记录
      component: recordGameList,
      name: 'recordGameList'
    },

    {
      path: '/wap/transferRecord',
      component: transferRecord,
      name: 'transferRecord'
    },
    {
      path: '/wap/record/withdrawals',
      component: record,
      name: 'withdrawal'
    },
    {
      path: '/wap/thirdParty',
      component: thirdParty,
      name: 'thirdParty'
    },

    ///wap/memberCenter/rescue

    {
      path: '/wap/memberCenter/rescue',
      component: rescue,
      name: 'rescue'
    },

    //thirdParty

    { // onlinepay   支付 首页
      path: '/wap/memberCenter/deposit',
      component: payHead,
      name: 'payHead'
    },
    { // onlinepay   网银支付 快捷支付页面
      path: '/wap/memberCenter/quickpay',
      component: quickpay,
      name: 'quickpay'
    },
    { //   'alipay': '支付宝',
      path: '/wap/memberCenter/alipay',
      component: scanpay,
      name: 'alipay'
    },
    {
      path: '/wap/memberCenter/jdpay',
      component: scanpay,
      name: 'jdpay'
    },
    { // 微信扫码
      path: '/wap/memberCenter/qqpay',
      component: qqpay,
      name: 'qqpay'
    },
    {
      path: '/wap/memberCenter/swiftpay',
      component: swiftpay,
      name: 'swiftpay'
    },
    { // 订单详情
      path: '/wap/memberCenter/orderDetail',
      component: orderDetail,
      name: 'orderDetail'
    },
    { // 订单详情 第三方的详情
      path: '/wap/memberCenter/orderDetail3',
      component: orderDetail3,
      name: 'orderDetail3'
    },
    { // 微信扫码
      path: '/wap/memberCenter/onlinepay',
      component: onlinepay,
      name: 'onlinepay'
    },
    { // 微信扫码
      path: '/wap/memberCenter/wxpayScan',
      component: wxpayScan,
      name: 'wxpayScan'
    },
    { // cancelSuccess   取消订单
      path: '/wap/memberCenter/cancelSuccess',
      component: cancelSuccess,
      name: 'cancelSuccess'
    }
  ]
}
