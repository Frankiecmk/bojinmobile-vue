import Vue    from 'vue'
import Router from 'vue-router'
import _      from 'lodash'

import home   from '@/components/home'
import center from '@/router/center'
import page   from './page'

window._ = _

Vue.use(Router)

/**
 *  全局路由配置
 *
 *  默认 走 wap
 *
 *  _.lodash()
 *
 *
 */
let routerPath = [{
  path: '/',
  name: 'home',
  component: home, // home 抽离出去
  children: _.concat([],
    page.pageRoutes,
    center.centerRoutes,
      // ......
      // .... 其他 path  还没有写 index 的路径？ 报错的缘故
      [{
      path: '/*',
      redirect: '/wap/index'
    }]
  )
}]

window.AppRouter = new Router({
  routes: routerPath,
  mode: 'history',
})
window.AppRouter.beforeEach((to, from, next) => {
  next()
  // do something ......
  console.log('页面刷新', window.AppRouter.app._route)
})
export default window.AppRouter
