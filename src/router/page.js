import mainIndex from './../components/home/mainIndex.vue'
import divMain from './../components/divMain'
import reg from './../components/home/reg'
import login from './../components/home/login'
import help from './../components/home/help'
import egame from './../components/home/egame'
import egameList from './../components/home/egameList'
import activity from './../components/home/activity'
import download from './../components/home/download'
import forgetPassword from  './../components/home/forgetPassword'
import message from  './../components/home/message'
import phoneRetrieve from  './../components/home/phoneRetrieve'



export default {
  pageRoutes: {
    path: '/wap',
    component: divMain,
    name: 'home',
    children: [
      {
        path: 'index',
        component: mainIndex,
        name: 'index'
      },
      {
        path: 'reg',
        component: reg,
        name: 'reg'
      },
      {
        path: 'login',
        component: login,
        name: 'login'
      },
      {
        path: 'phoneRetrieve', // 找回密码
        component: phoneRetrieve,
        name: 'phoneRetrieve'
      },
      {
        path: 'message',
        component: message,
        name: 'message'
      },
      {
        path: 'help',
        component: help,
        name: 'help'
      },
      {
        path: 'forgetPassword',
        component: forgetPassword,
        name: 'forgetPassword'
      },
      {
        path: 'egame',
        component: egame,
        name: 'egame'
      },
      {
        path: 'mg',
        component: egameList,
        name: 'mg'
      },
      {
        path: 'pt',
        component: egameList,
        name: 'pt'
      },
      {
        path: 'ttg',
        component: egameList,
        name: 'ttg'
      },
      {
        path: 'hb',
        component: egameList,
        name: 'hb'
      },
      {
        path: 'dt',
        component: egameList,
        name: 'dt'
      },
      {
        path: 'ae',
        component: egameList,
        name: 'egameList'
      },

      //
      {
        path: 'activity',
        component: activity,
        name: 'activity'
      },
      {
        path: 'download',
        component: download,
        name: 'download'
      }
    ]
  }
}
