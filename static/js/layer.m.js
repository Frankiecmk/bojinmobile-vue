;!(function (a) { 'use strict'; var b = ''; b = b || document.scripts[document.scripts.length - 1].src.match(/[\s\S]*\//)[0]; var c = document, d = 'querySelectorAll', e = 'getElementsByClassName', f = function (a) { return c[d](a) }; var g = {type: 0, shade: !0, shadeClose: !0, fixed: !0, anim: !0}; a.ready = {extend: function (a) { var b = JSON.parse(JSON.stringify(g)); for (var c in a)b[c] = a[c]; return b }, timer: {}, end: {}}; var h = 0, i = ['layermbox'], j = function (a) { var b = this; b.config = ready.extend(a), b.view() }; j.prototype.view = function () { var a = this, b = a.config, d = c.createElement('div'); a.id = d.id = i[0] + h, d.setAttribute('class', i[0] + ' ' + i[0] + (b.type || 0)), d.setAttribute('index', h); var g = (function () { var a = typeof b.title === 'object'; return b.title ? '<h3 style="' + (a ? b.title[1] : '') + '">' + (a ? b.title[0] : b.title) + '</h3>' : '' }()), j = (function () { var a, c = (b.btn || []).length; return c !== 0 && b.btn ? (a = '<span type="1">' + b.btn[0] + '</span>', c === 2 && (a = '<span type="0">' + b.btn[1] + '</span>' + a), '<div class="layermbtn">' + a + '</div>') : '' }()); if (b.fixed || (b.top = b.hasOwnProperty('top') ? b.top : 100, b.style = b.style || '', b.style += ' top:' + (c.body.scrollTop + b.top) + 'px'), b.type === 2 && (b.content = '<i></i><i class="laymloadtwo"></i><i></i><div>' + (b.content || '') + '</div>'), d.innerHTML = (b.shade ? '<div ' + (typeof b.shade === 'string' ? 'style="' + b.shade + '"' : '') + ' class="laymshade"></div>' : '') + '<div class="layermmain" ' + (b.fixed ? '' : 'style="position:static;"') + '><div class="section"><div class="layermchild ' + (b.className ? b.className : '') + ' ' + (b.type || b.shade ? '' : 'layermborder ') + (b.anim ? 'layermanim' : '') + '" ' + (b.style ? 'style="' + b.style + '"' : '') + '>' + g + '<div class="layermcont">' + b.content + '</div>' + j + '</div></div></div>', !b.type || b.type === 2) { var l = c[e](i[0] + b.type), m = l.length; m >= 1 && k.close(l[0].getAttribute('index')) }document.body.appendChild(d); var n = a.elem = f('#' + a.id)[0]; setTimeout(function () { try { n.className = n.className + ' layermshow' } catch (a) { return }b.success && b.success(n) }, 1), a.index = h++, a.action(b, n) }, j.prototype.action = function (a, b) { var c = this; if (a.time && (ready.timer[c.index] = setTimeout(function () { k.close(c.index) }, 1e3 * a.time)), a.btn) for (var d = b[e]('layermbtn')[0].children, f = d.length, g = 0; f > g; g++)d[g].onclick = function () { var b = this.getAttribute('type'); b == 0 ? (a.no && a.no(), k.close(c.index)) : a.yes ? a.yes(c.index) : k.close(c.index) }; if (a.shade && a.shadeClose) { var h = b[e]('laymshade')[0]; h.onclick = function () { k.close(c.index, a.end) }, h.ontouchmove = function () { k.close(c.index, a.end) } }a.end && (ready.end[c.index] = a.end) }; var k = {v: '1.5', index: h, open: function (a) { var b = new j(a || {}); return b.index }, close: function (a) { var b = f('#' + i[0] + a)[0]; b && (b.innerHTML = '', c.body.removeChild(b), clearTimeout(ready.timer[a]), delete ready.timer[a], typeof ready.end[a] === 'function' && ready.end[a](), delete ready.end[a]) }, closeAll: function () { for (var a = c[e](i[0]), b = 0, d = a.length; d > b; b++)k.close(0 | a[0].getAttribute('index')) }}; typeof define === 'function' ? define(function () { return k }) : a.layer = k }(window))
!function(n){"use strict";function t(n,t){var r=(65535&n)+(65535&t);return(n>>16)+(t>>16)+(r>>16)<<16|65535&r}function r(n,t){return n<<t|n>>>32-t}function e(n,e,o,u,c,f){return t(r(t(t(e,n),t(u,f)),c),o)}function o(n,t,r,o,u,c,f){return e(t&r|~t&o,n,t,u,c,f)}function u(n,t,r,o,u,c,f){return e(t&o|r&~o,n,t,u,c,f)}function c(n,t,r,o,u,c,f){return e(t^r^o,n,t,u,c,f)}function f(n,t,r,o,u,c,f){return e(r^(t|~o),n,t,u,c,f)}function i(n,r){n[r>>5]|=128<<r%32,n[14+(r+64>>>9<<4)]=r;var e,i,a,d,h,l=1732584193,g=-271733879,v=-1732584194,m=271733878;for(e=0;e<n.length;e+=16)i=l,a=g,d=v,h=m,g=f(g=f(g=f(g=f(g=c(g=c(g=c(g=c(g=u(g=u(g=u(g=u(g=o(g=o(g=o(g=o(g,v=o(v,m=o(m,l=o(l,g,v,m,n[e],7,-680876936),g,v,n[e+1],12,-389564586),l,g,n[e+2],17,606105819),m,l,n[e+3],22,-1044525330),v=o(v,m=o(m,l=o(l,g,v,m,n[e+4],7,-176418897),g,v,n[e+5],12,1200080426),l,g,n[e+6],17,-1473231341),m,l,n[e+7],22,-45705983),v=o(v,m=o(m,l=o(l,g,v,m,n[e+8],7,1770035416),g,v,n[e+9],12,-1958414417),l,g,n[e+10],17,-42063),m,l,n[e+11],22,-1990404162),v=o(v,m=o(m,l=o(l,g,v,m,n[e+12],7,1804603682),g,v,n[e+13],12,-40341101),l,g,n[e+14],17,-1502002290),m,l,n[e+15],22,1236535329),v=u(v,m=u(m,l=u(l,g,v,m,n[e+1],5,-165796510),g,v,n[e+6],9,-1069501632),l,g,n[e+11],14,643717713),m,l,n[e],20,-373897302),v=u(v,m=u(m,l=u(l,g,v,m,n[e+5],5,-701558691),g,v,n[e+10],9,38016083),l,g,n[e+15],14,-660478335),m,l,n[e+4],20,-405537848),v=u(v,m=u(m,l=u(l,g,v,m,n[e+9],5,568446438),g,v,n[e+14],9,-1019803690),l,g,n[e+3],14,-187363961),m,l,n[e+8],20,1163531501),v=u(v,m=u(m,l=u(l,g,v,m,n[e+13],5,-1444681467),g,v,n[e+2],9,-51403784),l,g,n[e+7],14,1735328473),m,l,n[e+12],20,-1926607734),v=c(v,m=c(m,l=c(l,g,v,m,n[e+5],4,-378558),g,v,n[e+8],11,-2022574463),l,g,n[e+11],16,1839030562),m,l,n[e+14],23,-35309556),v=c(v,m=c(m,l=c(l,g,v,m,n[e+1],4,-1530992060),g,v,n[e+4],11,1272893353),l,g,n[e+7],16,-155497632),m,l,n[e+10],23,-1094730640),v=c(v,m=c(m,l=c(l,g,v,m,n[e+13],4,681279174),g,v,n[e],11,-358537222),l,g,n[e+3],16,-722521979),m,l,n[e+6],23,76029189),v=c(v,m=c(m,l=c(l,g,v,m,n[e+9],4,-640364487),g,v,n[e+12],11,-421815835),l,g,n[e+15],16,530742520),m,l,n[e+2],23,-995338651),v=f(v,m=f(m,l=f(l,g,v,m,n[e],6,-198630844),g,v,n[e+7],10,1126891415),l,g,n[e+14],15,-1416354905),m,l,n[e+5],21,-57434055),v=f(v,m=f(m,l=f(l,g,v,m,n[e+12],6,1700485571),g,v,n[e+3],10,-1894986606),l,g,n[e+10],15,-1051523),m,l,n[e+1],21,-2054922799),v=f(v,m=f(m,l=f(l,g,v,m,n[e+8],6,1873313359),g,v,n[e+15],10,-30611744),l,g,n[e+6],15,-1560198380),m,l,n[e+13],21,1309151649),v=f(v,m=f(m,l=f(l,g,v,m,n[e+4],6,-145523070),g,v,n[e+11],10,-1120210379),l,g,n[e+2],15,718787259),m,l,n[e+9],21,-343485551),l=t(l,i),g=t(g,a),v=t(v,d),m=t(m,h);return[l,g,v,m]}function a(n){var t,r="",e=32*n.length;for(t=0;t<e;t+=8)r+=String.fromCharCode(n[t>>5]>>>t%32&255);return r}function d(n){var t,r=[];for(r[(n.length>>2)-1]=void 0,t=0;t<r.length;t+=1)r[t]=0;var e=8*n.length;for(t=0;t<e;t+=8)r[t>>5]|=(255&n.charCodeAt(t/8))<<t%32;return r}function h(n){return a(i(d(n),8*n.length))}function l(n,t){var r,e,o=d(n),u=[],c=[];for(u[15]=c[15]=void 0,o.length>16&&(o=i(o,8*n.length)),r=0;r<16;r+=1)u[r]=909522486^o[r],c[r]=1549556828^o[r];return e=i(u.concat(d(t)),512+8*t.length),a(i(c.concat(e),640))}function g(n){var t,r,e="";for(r=0;r<n.length;r+=1)t=n.charCodeAt(r),e+="0123456789abcdef".charAt(t>>>4&15)+"0123456789abcdef".charAt(15&t);return e}function v(n){return unescape(encodeURIComponent(n))}function m(n){return h(v(n))}function p(n){return g(m(n))}function s(n,t){return l(v(n),v(t))}function C(n,t){return g(s(n,t))}function A(n,t,r){return t?r?s(t,n):C(t,n):r?m(n):p(n)}"function"==typeof define&&define.amd?define(function(){return A}):"object"==typeof module&&module.exports?module.exports=A:n.md5=A}(this);
console.info('layer')

// add other code

;(function () {

  var IEvar = '9'
  var IEnum = navigator.userAgent.toLowerCase()

  if (IEnum.indexOf('msie') > -1) {
    if (Number(IEnum.match(/msie ([\d.]+)/)[1]) < IEvar) {

      alert('您的浏览器太旧或者开启了隐私模式/无痕模式，无法浏览网页，请更换浏览器或使用常规模式，给您带来的不便，表示抱歉！！')
      window.opener = null
      window.open('', '_self', '')
      window.close()
    }
    return false
  }
  window[ addEventListener ? 'addEventListener' : 'attachEvent' ](addEventListener ? 'load' : 'onload', function () {
    setTimeout(function () {
      try {
        window.localStorage.setItem('T', '1')
        window.localStorage.removeItem('T')
      } catch (e) {
        alert('您的浏览器太旧或者开启了隐私模式/无痕模式，无法浏览网页，请更换浏览器或使用常规模式，给您带来的不便，表示抱歉！！！')
        window.opener = null
        window.open('', '_self', '')
        window.close()
      }
    }, 0)
  })
})();
(function () {
  // Promise是否能用
  if (typeof (Promise) === 'undefined') {
    document.write(
      '<script src="//cdn.bootcss.com/es6-promise/4.0.5/es6-promise.auto.min.js"><\/script>')
  }
  // fetch是否能用
  if (typeof (fetch) === 'undefined') {
    document.write('<script src="//cdn.bootcss.com/fetch/2.0.1/fetch.min.js"><\/script>')
  }
})()
